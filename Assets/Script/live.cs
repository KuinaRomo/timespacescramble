﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class live : MonoBehaviour {

    private GameObject GameControler;
	private GameObject[] parts;

    public int playerLive;
    private bool inmortality;
	private float count;
    public float countInvisibility;
    public bool invisibility;
    public float quitarPoner;

	// Use this for initialization
	void Start () {
        GameControler = GameObject.FindGameObjectWithTag("GameControler");

        playerLive = 10;
        inmortality = false;
        invisibility = false;
        countInvisibility = 0.0f;
        quitarPoner = 0.025f;
		parts = GameObject.FindGameObjectsWithTag ("Parts");
    }
	
	// Update is called once per frame
	void Update () {
		if (inmortality == true){
            if (countInvisibility < 0.4f){
                countInvisibility += quitarPoner;
            }
            else{
                if (invisibility){
                    invisibility = false;
                }
                else{
                    invisibility = true;
                }
                countInvisibility = 0.0f;
            }

            if (invisibility == false){
				System.Array.Clear (parts, 0, parts.Length);
				parts = GameObject.FindGameObjectsWithTag ("Parts");
				foreach(GameObject part in parts){
					part.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 0);
				}
				GameObject.FindGameObjectWithTag("center").GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 0);
            }
            else{
				foreach(GameObject part in parts){
					part.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
				}
				GameObject.FindGameObjectWithTag("center").GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
			}

            count += Time.deltaTime;
            if(count > 1){
                inmortality = false;
                count = 0;
				foreach(GameObject part in parts){
					part.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
				}
				GameObject.FindGameObjectWithTag("center").GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
			}
        }
    }

    private void OnTriggerStay2D(Collider2D collision) {
		if(collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "bala"){
            if(!inmortality){
                inmortality = true;
                playerLive--;
				GameControler.GetComponent<Sigleton>().PlayerLiveIngame = playerLive;
            }
        }
    }


	void OnCollisionEnter2D(Collision2D death){
		if (death.gameObject.tag == "Death") {
			playerLive = 0;
			GameControler.GetComponent<Sigleton>().PlayerLiveIngame = playerLive;
		}
	}
}
