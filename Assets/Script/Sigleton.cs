﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Sigleton : MonoBehaviour {

    public int PlayerLiveIngame;
    public int PlayerLive;
    private GameObject Player;
	private GameObject[] Enemies;
	private GameObject[] Pause;

	private float playerX;
	private float playerY;

	public bool restartValues;
	private bool pausa;
    
	// Use this for initialization
	void Start () {
		Enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		Player = GameObject.FindGameObjectWithTag("Player");
		Pause = GameObject.FindGameObjectsWithTag ("Pause");
		PlayerLiveIngame = 10;
        PlayerLive = 3;
		playerX = Player.transform.position.x;
		playerY = Player.transform.position.y;
    }

	
	// Update is called once per frame
	void Update () {
		if (PlayerLiveIngame == 0) {
			Player.GetComponent<live> ().playerLive = 10;
			PlayerLiveIngame = 10;
			PlayerLive--;
			if (PlayerLive == 0) {
				SceneManager.LoadScene("Menu");
			} 
			else {
				Player.GetComponent<Moviment> ().initValues = true;
				Player.transform.position = new Vector3 (playerX, playerY, 0);
				foreach (GameObject enemy in Enemies) {
					enemy.GetComponent<Enemy> ().live = 5;
					enemy.SetActive (true);
				}
			}
		}
		if (restartValues) {
			ResetValues();
			restartValues = false;
		}

		//Pausar al apretar p
		if(Input.GetKeyDown(KeyCode.P)){
			if (pausa) {
				pausa = false;
				Time.timeScale = 1;
			} 
			else {
				pausa = true;
				Time.timeScale = 0;
			}
		}

		//Controla Pausa del juego
		if (!pausa) {
			foreach (GameObject obPau in Pause) {
				obPau.gameObject.SetActive (false);
			}
		} 
		else {
			foreach (GameObject obPau in Pause) {
				obPau.gameObject.SetActive (true);
			}
		}
    }

	void ResetValues(){
		Enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		Player = GameObject.FindGameObjectWithTag("Player");
		PlayerLiveIngame = 10;
		playerX = Player.transform.position.x;
		playerY = Player.transform.position.y;
	}
}

