﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour {

	private int speed;
	private GameObject player;
	private Rigidbody2D rb2D;
	private GameObject camara;

	// Use this for initialization
	void Start () {
		camara = GameObject.FindGameObjectWithTag("MainCamera");
		player = GameObject.FindGameObjectWithTag("Player");
		rb2D = this.GetComponent<Rigidbody2D> ();
		if (transform.position.x < player.transform.position.x) {
			speed = 2;
		}
		else if(transform.position.x > player.transform.position.x) {
			speed = -2;
		}
	}
	
	// Update is called once per frame
	void Update () {
		rb2D.AddForce(new Vector3(speed, 0, 0));
		if (transform.position.x < camara.transform.position.x - 10 || transform.position.x > camara.transform.position.x + 10) {
			Destroy (this.gameObject);
		}
	}

	/*void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "bulletDestroy") {
			Destroy (this.gameObject);	
		}
	}*/
}
