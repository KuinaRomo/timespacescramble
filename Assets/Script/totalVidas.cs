﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class totalVidas : MonoBehaviour {

    private GameObject GameController;
    private int Live;
	    
	// Use this for initialization
	void Start () {
        GameController = GameObject.FindGameObjectWithTag("GameControler");
        Live = GameController.GetComponent<Sigleton>().PlayerLive;
	}
	
	// Update is called once per frame
	void Update () {
		Live = GameController.GetComponent<Sigleton>().PlayerLive;
		this.GetComponent<Text> ().text = "X " + Live;
	}
}
