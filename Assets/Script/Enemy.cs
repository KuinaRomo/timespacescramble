﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int live;
	public bool dado;
    
	// Use this for initialization
	void Start () {
        live = 5;
		dado = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (live == 0){
			dado = false;
			this.gameObject.SetActive(false);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision){
        if(collision.gameObject.tag == "Sword"){
            live--;
			dado = true;
        }
    }
}
