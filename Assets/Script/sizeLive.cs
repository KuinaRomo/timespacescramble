﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sizeLive : MonoBehaviour {

    private GameObject GameControler;
	private int playerLive;
    private float sizeOfLive;

    // Use this for initialization
    void Start () {
        GameControler = GameObject.FindGameObjectWithTag("GameControler");
	}
	
	// Update is called once per frame
	void Update () {
        playerLive = GameControler.GetComponent<Sigleton>().PlayerLiveIngame;
        sizeOfLive = (128.0f / 10.0f)*playerLive;
        RectTransform rt = this.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(sizeOfLive, 12);
    }
}
