﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patroller : MonoBehaviour {

	private float speed;
	private Rigidbody2D rb2D;

	// Use this for initialization
	void Start () {
		speed = 0.5f;
		rb2D = this.GetComponent<Rigidbody2D> ();
		this.GetComponent<Animator> ().SetBool ("right", true);
	}
	
	// Update is called once per frame
	void Update () {
		rb2D.AddForce(new Vector3(speed, 0, 0));
	}

	private void OnTriggerEnter2D(Collider2D collision){
		if(collision.gameObject.tag == "turn"){
			rb2D.Sleep();
			speed = speed * -1;
			if (speed < 0) {
				this.GetComponent<Animator> ().SetBool ("right", false); 			
			} 
			else {
				this.GetComponent<Animator> ().SetBool ("right", true); 
			}
		}
	}
}
