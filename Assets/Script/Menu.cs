﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour {

	private float[] numY = new float[2]{-0.24f, -1.09f};
	private int contadorY = 0;

	// Use this for initialization
	void Start () {
		Debug.Log (numY [0]);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("down")) {
			if (contadorY != 2) {
				contadorY++;
			}
			transform.position = new Vector2 (transform.position.x, numY [contadorY]);
		}
		if (Input.GetKeyDown ("up")) {
			if (contadorY != 0) {
				contadorY--;
			}
			transform.position = new Vector2 (transform.position.x, numY [contadorY]);
		}
		if (Input.GetKeyDown (KeyCode.Return)) {
			if (contadorY == 0) {
				SceneManager.LoadScene ("Level1");
			} 
			else if (contadorY == 1) {
				//SceneManager.LoadScene("Game");
				Debug.Log ("Cargar menu de partidas guardadas si hay");
			} 
			else {
				Debug.Log ("Exit Game");
				Application.Quit();
			}
		}
	}
}
