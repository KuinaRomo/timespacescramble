﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

    private GameObject Camara;
    private GameObject Player;
    private GameObject Enemy;

    private bool attack;

    private float count;
	private AudioSource audio;

    // Use this for initialization
    void Start () {
        Camara = GameObject.FindGameObjectWithTag("MainCamera");
        Player = GameObject.FindGameObjectWithTag("Player");
        Enemy = GameObject.FindGameObjectWithTag("Enemy");
        attack = false;
        count = 0;
		audio = this.GetComponent<AudioSource> ();
    }
	
	// Update is called once per frame
	void Update () {
        if(attack == false){
            transform.position = new Vector3(Camara.transform.position.x - 10, Camara.transform.position.y, transform.position.z);
        }
        else{
			if(Player.GetComponent<Moviment>().dir == "Izq" || Player.GetComponent<Moviment>().lastDir == "Izq"){
                transform.position = new Vector3(Player.transform.position.x - 3f, Player.transform.position.y, transform.position.z);
            }
            else{
                transform.position = new Vector3(Player.transform.position.x + 3f, Player.transform.position.y, transform.position.z);
            }
            count += Time.deltaTime;
            if(count > 0.5f){
                attack = false;
                count = 0;
                Enemy.GetComponent<Enemy>().dado = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Z)){
            attack = true;
			audio.Play ();
        }
    }
}
