﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooter : MonoBehaviour {

	private float temp;
	public GameObject bala;
	public GameObject Player;
	private string dir;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		temp = 0.0f;
		Player = GameObject.FindGameObjectWithTag ("Player");
		audio = this.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (temp < 2.5f) {
			temp += Time.deltaTime;
		} 
		else {
			Instantiate (bala);
			audio.Play ();
			bala.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z);
			temp = 0;
		}

		if (Player.transform.position.x < transform.position.x) {
			if (transform.localScale.x > 0) {
				transform.localScale = new Vector3 (transform.localScale.x*(-1), transform.localScale.y, transform.localScale.z);
			}
		}
		else if (Player.transform.position.x > transform.position.x) {
			if (transform.localScale.x < 0) {
				transform.localScale = new Vector3 (transform.localScale.x*(-1), transform.localScale.y, transform.localScale.z);
			}
		}
	}
}
