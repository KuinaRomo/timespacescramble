﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paralax : MonoBehaviour {

	GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3 (player.transform.position.x - 0.5f, player.transform.position.y + 1.0f, transform.position.z);
	}
}
