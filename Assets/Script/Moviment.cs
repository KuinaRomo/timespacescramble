﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Moviment : MonoBehaviour {

    private int teleportDistance;
	private float speed;
	private float horizontal;
	private float vertical;
	//public float maxS = 5f;
	//public float move = 0f;
    public float velocity;
	private float count;
	public int total;

    private Rigidbody2D rb2D;
    private string[] directions;
    private string[] changeDir;
    private string[] colors;
	private int directionInt;
    private int translateDir;

    public int jumpY;
    public int jumpX;
	public float beltSpeed;
    private float countTeleport;
	private float countAttack;
	public float totalTimeAttack;

    private bool jump;
    public bool visualBool;
    private bool mTeleport;
    private bool changed;
	public bool initValues;
	private bool beltLeft;
	private bool beltRight;
	private bool teleportNow;
	private bool attack;
    
    public string dir;
	public string lastDir;

    private Sprite prueba;
	private GameObject GameControler;
	private GameObject aux;
	private GameObject aux2;
	private GameObject effect;
	private GameObject[] parts;

    // Use this for initialization
    void Start () {
		rb2D = GetComponent<Rigidbody2D> ();

		directions = new string[] { "Izq", "Der", "Up", "Down" };
        colors = new string[] { "lila", "azul", "rojo", "verde" };
		lastDir = "none";
		dir = "none";

		changeDir = new string[4];
        speed = 6;
		beltSpeed = 3f;
        directionInt = 0;
        translateDir = 0;
        teleportDistance = 10;
        velocity = 0f;
        jumpX = 200;
        jumpY = 450;
		total = 1;
		totalTimeAttack = 0.5f;
		countAttack = 0;


        jump = false;
        mTeleport = false;
        changed = false;
		initValues = false;
		beltLeft = false;
		beltRight = false;
		teleportNow = false;

		GameControler = GameObject.FindWithTag ("GameControler");
		GameControler.GetComponent<Sigleton> ().restartValues = true;
		parts = GameObject.FindGameObjectsWithTag ("Parts");
		effect = GameObject.FindGameObjectWithTag ("Effect");
	}
	
	// Update is called once per frame
	void Update () {
		if (attack) {
			countAttack += Time.deltaTime;
			if (countAttack > totalTimeAttack) {
				attack = false;
				System.Array.Clear (parts, 0, parts.Length);
				parts = GameObject.FindGameObjectsWithTag ("Parts");
				int aux = 0;
				foreach (GameObject part in parts) {
					part.GetComponent<Animator> ().SetBool ("attackRight", false);
					part.GetComponent<Animator> ().SetBool ("attackLeft", false);
				}
				GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("attackLeft", false);
				GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("attackRight", false);
				countAttack = 0;
				attack = false;
			}
		}
		//Reset teleport
        if(mTeleport == true){
            countTeleport += Time.deltaTime;
            if(countTeleport > 1){
                countTeleport = 0;
                mTeleport = false;
            }
        }
		if (initValues) {
			initValues = false;
			restoreValues ();
		}

		if (teleportNow) {
			count += Time.deltaTime;
			if (count > total) {
				effect.transform.position = new Vector3 (5000000, 500000, 500000);
				effect.GetComponent<Animator> ().SetBool ("teleport", false);
				count = 0;
				teleportNow = false;
			} 
			else {
				effect.transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z - 1);
			}
		}
	}

	void FixedUpdate () {
		if(Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow)){
			if (dir != "none") {
				lastDir = dir;
			}
			dir = "none";
			System.Array.Clear (parts, 0, parts.Length);
			parts = GameObject.FindGameObjectsWithTag ("Parts");
			foreach(GameObject part in parts){
				part.GetComponent<Animator> ().SetBool ("right", false);
				part.GetComponent<Animator> ().SetBool ("left", false);
			}
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("right", false);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("left", false);
		}

		if (!jump) {
			System.Array.Clear (parts, 0, parts.Length);
			parts = GameObject.FindGameObjectsWithTag ("Parts");
			foreach(GameObject part in parts){
				part.GetComponent<Animator> ().SetBool ("jumpRight", false);
				part.GetComponent<Animator> ().SetBool ("jumpLeft", false);
			}
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("jumpRight", false);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("jumpLeft", false);
		}

		/*if (dir == "Der" || lastDir == "Der" && dir != "Izq") {
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("right", true);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("left", false);
		} 
		else {
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("left", true);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("right", false);
		}*/

        if (Input.GetKey(KeyCode.LeftArrow)) {
			directionInt = 0;
            if (jump == false){
                translateDirections(directionInt);

            }
        }
        if (Input.GetKey(KeyCode.RightArrow)) {
			directionInt = 1;
            if (jump == false){
                translateDirections(directionInt);

            }
        }
        if (Input.GetKey(KeyCode.UpArrow)) {
            directionInt = 2;
            if (jump == false){
                translateDirections(directionInt);
            }
        }
        if (Input.GetKey(KeyCode.DownArrow)){
            directionInt = 3;
            if (jump == false){
                translateDirections(directionInt);
            }
        }

		//Teleport en cintas
		if (beltLeft) {
			transform.position = new Vector3 (transform.position.x - beltSpeed, transform.position.y, transform.position.z);
		} 
		else if (beltRight) {
			transform.position = new Vector3 (transform.position.x + beltSpeed, transform.position.y, transform.position.z);
		}


        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow) || jump == true){
            if(GetComponent<Rigidbody2D>().velocity.y == 0){
				if (!beltLeft && !beltRight) {
					rb2D.Sleep();
				} 
            }
        }
        // ---------------------------- Salto ------------------------
        if (Input.GetKeyDown(KeyCode.Space) && !jump){
            jump = true;
			System.Array.Clear (parts, 0, parts.Length);
			parts = GameObject.FindGameObjectsWithTag ("Parts");
			if (dir == "Izq" || lastDir == "Izq"){
                rb2D.AddForce(new Vector3(-jumpX, jumpY, 0));
				foreach(GameObject part in parts){
					part.GetComponent<Animator> ().SetBool ("left", false);
					part.GetComponent<Animator> ().SetBool ("jumpLeft", true);
				}
				GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("left", false);
				GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("jumpLeft", true);
            }
			else if (dir == "Der" || lastDir == "Der" || lastDir == "none"){
                rb2D.AddForce(new Vector3(jumpX, jumpY, 0));
				foreach(GameObject part in parts){
					part.GetComponent<Animator> ().SetBool ("right", false);
					part.GetComponent<Animator> ().SetBool ("jumpRight", true);
				}
				GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("right", false);
				GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("jumpRight", true);
            }
            else{
                rb2D.AddForce(new Vector3(0, jumpY, 0));
            }
        }

        //----------------------------- Teleport --------------------------------
        if (Input.GetKey(KeyCode.V)){
			effect.transform.position = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			effect.GetComponent<Animator> ().SetBool ("teleport", true);
			teleportNow = true;
			if (Input.GetKey(KeyCode.LeftArrow)){
                teleport(0);
            }
            else if (Input.GetKey(KeyCode.RightArrow)){
                teleport(1);
            }
            else if (Input.GetKey(KeyCode.UpArrow)){
                teleport(2);
            }
            else if (Input.GetKey(KeyCode.DownArrow)){
                teleport(3);
            }
        }

		if (Input.GetKeyDown (KeyCode.Z)) {
			System.Array.Clear (parts, 0, parts.Length);
			parts = GameObject.FindGameObjectsWithTag ("Parts");
			int aux = 0;
			foreach(GameObject part in parts){
				if (dir == "Der" || lastDir == "Der" || lastDir == "none") {
					aux++;
					if (aux != 2 || dir == "none") {
						part.GetComponent<Animator> ().SetBool ("right", false);
						part.GetComponent<Animator> ().SetBool ("attackRight", true);
						attack = true;
					}
					GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("right", false);
					GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("attackRight", true);
				} 
				else {
					aux++;
					if (aux != 2 || dir == "none") {
						part.GetComponent<Animator> ().SetBool ("left", false);
						part.GetComponent<Animator> ().SetBool ("attackLeft", true);
						attack = true;
					}
					GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("left", false);
					GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("attackLeft", true);
				}
			}
		}
    }

    private void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.tag == "floor"){
			Debug.Log ("Entro");
            jump = false;
        }
		if(collision.gameObject.tag == "Final"){
			//DontDestroyOnLoad (GameControler);
			//DontDestroyOnLoad (GameObject.FindGameObjectWithTag ("Info"));
			SceneManager.LoadScene("Menu");
		}
    }

	private void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "cintaLeft") {
			rb2D.AddForce (new Vector3 (-beltSpeed, 0, 0));
			beltLeft = true;
		}
		else if (collider.gameObject.tag == "cintaRight") {
			rb2D.AddForce (new Vector3 (beltSpeed, 0, 0));
			beltRight = true;
		}
	}

	private void OnTriggerExit2D(Collider2D collider){
		if (collider.gameObject.tag == "cintaLeft") {
			beltLeft = false;
		}
		else if (collider.gameObject.tag == "cintaRight") {
			beltRight = false;
		}
	}

	void translateDirections(int num) {
        if (directions[num] == "Izq") {
			System.Array.Clear (parts, 0, parts.Length);
			parts = GameObject.FindGameObjectsWithTag ("Parts");
			foreach(GameObject part in parts){
				part.GetComponent<Animator> ().SetBool ("right", false);
				part.GetComponent<Animator> ().SetBool ("iddleLeft", true);
				part.GetComponent<Animator> ().SetBool ("left", true);
			}
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("right", false);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("iddleLeft", true);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("left", true);
			rb2D.AddForce(new Vector3(-speed, 0, 0));
			dir = "Izq";
        }
        else if(directions[num] == "Der") {
			System.Array.Clear (parts, 0, parts.Length);
			parts = GameObject.FindGameObjectsWithTag ("Parts");
			foreach(GameObject part in parts){
				part.GetComponent<Animator> ().SetBool ("left", false);
				part.GetComponent<Animator> ().SetBool ("iddleLeft", false);
				part.GetComponent<Animator> ().SetBool ("right", true);
			}
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("left", false);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("iddleLeft", false);
			GameObject.FindGameObjectWithTag ("center").GetComponent<Animator> ().SetBool ("right", true);
			rb2D.AddForce(new Vector3(speed, 0, 0));
			dir = "Der";
        }
	}

	  void changeControls() {
		for(int i = 0; i < changeDir.Length; i++){
            changeDir[i] = null;
        }
        translateDir = 0;
        while (!changed){
            bool be = false;
            int randomNum = Random.Range(0, 4);
            for (int i = 0; i < changeDir.Length; i++){
                if(changeDir[i] == directions[randomNum]){
                    be = true;
                }
            }
            if(be == true){
                be = false;
            }
            else{
                changeDir[translateDir] = directions[randomNum];
                translateDir++;
            }
            if (translateDir == 4){
                bool diferent = false;
                for (int i = 0; i < changeDir.Length; i++){
                    if(changeDir[i] != directions[i]){
                        diferent = true;
                    }
                }
                if(diferent){
                    changed = true;
                }
                else{
                    translateDir = 0;
                    for (int i = 0; i < changeDir.Length; i++){
                        changeDir[i] = null;
                    }
                }
            }
        }
        changed = false;
		foreach(GameObject part in parts){
			Destroy(part);
		}
        for(int i = 0; i < changeDir.Length; i++){
            directions[i] = changeDir[i];
            string auxStr ="Prefabs/" + colors[i] + directions[i];
			GameObject robotPart = Instantiate (Resources.Load (auxStr), new Vector3 (transform.position.x, transform.position.y, transform.position.z), transform.rotation) as GameObject;
			robotPart.transform.parent = this.transform;
		}
		System.Array.Clear (parts, 0, parts.Length);
		parts = GameObject.FindGameObjectsWithTag ("Parts");
		translateDir = 0;
        directionInt = 0;
    }

    void teleport(int num){
        if (!mTeleport){
            mTeleport = true;
            Debug.Log("teleport");
            if(directions[num] == "Izq"){
                transform.position = new Vector3(transform.position.x - teleportDistance, transform.position.y, transform.position.z);
            }
            else if (directions[num] == "Der"){
                transform.position = new Vector3(transform.position.x + teleportDistance, transform.position.y, transform.position.z);
            }
            else if(directions[num] == "Down"){
                transform.position = new Vector3(transform.position.x, transform.position.y - teleportDistance, transform.position.z);
            }
            else{
                transform.position = new Vector3(transform.position.x, transform.position.y + teleportDistance, transform.position.z);
            }
            changeControls();
        }
    }

	void restoreValues(){
		directions[0] = "Izq";
		directions[1] = "Der";
		directions[2] = "Up";
		directions[3] = "Down";
		colors[0] = "lila";
		colors[1] = "azul";
		colors[2] = "rojo";
		colors[3] = "verde";

		foreach(GameObject part in parts){
			Destroy(part);
		}
		for(int i = 0; i < changeDir.Length; i++){
			string auxStr ="Prefabs/" + colors[i] + directions[i];
			GameObject robotPart = Instantiate (Resources.Load (auxStr), new Vector3 (transform.position.x, transform.position.y, transform.position.z), transform.rotation) as GameObject;
			robotPart.transform.parent = this.transform;
		}
		System.Array.Clear (parts, 0, parts.Length);
		parts = GameObject.FindGameObjectsWithTag ("Parts");
	}


}
